import { WebSocketServer } from 'ws';
const wss = new WebSocketServer({ port: 8080 });

//Make sure to use the player's instance ID from the Inspector's Debug mode, currently set to 17074 in this playerData Example
const playerData = `{"PlayerID":17074,"Pos":{"x":0.0,"y":0.0,"z":0.0},"Rot":{"x":0.0,"y":0.0,"z":0.0}}`;
const transactionsCount = 10000;
const playerCount = 10;

const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

var playersData = "";
for (let i = 0; i < playerCount; i++) 
{
   playersData += playerData;

   //Adding delimeter
   if(i < playerCount -1)
   playersData += ",";
}
playersData = `[${playersData}]`;

//on Started
wss.on('listening', () =>
{
  console.log(`server listening...`);
});

wss.on('connection', function connection(ws) 
{
   console.log('connected');
   ws.on('message', function incoming(message) 
   {
      if (message == "EOT") {
         console.log('received: %s', message);
         ws.close();
         console.log("closing connection");
      }

      else if(message == "Start") 
      {
         console.log(`rcvd: ${message}. Spamming ${wss.clients.size} client with ${transactionsCount} messages: ${playerData} x ${playerCount}`);
         for (let i = 0; i < transactionsCount; i++) 
         {
            wss.clients.forEach((client) =>
            {
               if (client.readyState == 1) 
               {
                  client.send(playersData); 
                  //sleep(1);//induced delay, remove if not in test
               }
            });
         }
      }
   });
});

