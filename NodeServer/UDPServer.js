const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const host = 'localhost';
const port = process.env.port || 5500;

const playerData = `{"PlayerID":17378,"Pos":{"x":0.0,"y":0.0,"z":0.0},"Rot":{"x":0.0,"y":0.0,"z":0.0}}`;
const testCount = 40;

//on Started
server.on('listening', () =>
{
  console.log(`server listening on ${host}:${port}`);
});

//on message received
server.on('message',async (msg, senderInfo) => 
{
  msg = msg.toString().toUpperCase();
  console.log(`${msg}. rcvd from: [${senderInfo.address}:${senderInfo.port}]`);
  switch (msg) {
    case 'TEST':    
      console.log(`stress test started @ [${testCount} msgs]`);

        for (let i = 0; i <= testCount; i++) 
        {
          console.log(`test[${i}]`);
          server.send(playerData,senderInfo.port,senderInfo.address);
          
          //closing connection
          // if(i >= testCount)
          //   server.close();
        }
        console.log('Test Ended')
        break;
    case 'TEST2':
      for (let i = 0; i <= testCount; i++) 
      {
        server.send(`Packet[${i}]`,senderInfo.port,senderInfo.address);
        console.log(`test2[${i}]`);
        for (let i = 0; i < 5; i++) 
          server.send(playerData,senderInfo.port,senderInfo.address);
        await Sleep(1000);
      }
      break;
    default: //echo
    //for (let i = 0; i <= testCount; i++) 
      server.send(Buffer.from(`Msg: ${msg}`),senderInfo.port,senderInfo.address);
  }
});

//error handling
server.on('error', (err) => 
{
  console.log(`server error:\n${err.stack}`);
  server.close();
});


async function Sleep(millis) {
  return new Promise(resolve => setTimeout(resolve, millis));
}
server.bind(port);