const WebSocket = require('ws')
const wss = new WebSocket.Server({ port: 8080 },()=>{
    console.log('server started')
})

const playerData = `{"PlayerID":17378,"Pos":{"x":0.0,"y":0.0,"z":0.0},"Rot":{"x":0.0,"y":0.0,"z":0.0}}`;
const testCount = 20;

wss.on('connection', function connection(ws) {
   ws.on('message', (message) => {
      console.log(`rcvd: ${message}. Spamming client with ${testCount} messages: ${playerData}`);
      for (let i = 0; i < testCount; i++) 
         ws.send(playerData);
   })
})
wss.on('listening',()=>{
   console.log('game server listening on 8080')
})