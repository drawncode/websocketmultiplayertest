using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class WSSimpleClient : MonoBehaviour
{
    public static List<PlayerData> PlayersData { get; private set; }
    private DateTime _startTime;
    string jsonExample = @"{""PlayerID"":15078,""Pos"":{""x"":0,""y"":1.85,""z"":0},""Rot"":{""x"":0,""y"":90,""z"":0}}";

    private string serverURL = $"localhost:54321/unity/";

    public static event Action<DateTime, int> OnFPS = delegate { };

    private IEnumerator Post(string url, string bodyJsonString)
    {
        var request = new UnityWebRequest(url, "GET");//POST
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        //request.SetRequestHeader("Content-type", "text/plain");
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(request.error, this);
        }
        else
        {
            string dataRcvd = request.downloadHandler.text;
            //Debug.Log("Rcvd: " + dataRcvd);
            ProcessServerResponse(dataRcvd);
        }
    }

    private void ProcessServerResponse(string rawResponse)
    {
        PlayersData = JsonConvert.DeserializeObject<List<PlayerData>>(rawResponse);
        int ctr = 0;
        //foreach (var playerData in playerData_Recieved)
        {
            ctr++;
            Debug.Log($"{rawResponse}[{ctr}]");
        }

        //LogTimeTaken(_startTime, ctr);
    }

    private void LogTimeTaken(DateTime startTime, int msgCount)
    {
        var endTime = DateTime.UtcNow;
        var elapsedTime = (endTime - startTime).TotalSeconds;

        var fps = Math.Round(msgCount / elapsedTime, 3);
        Debug.Log($"FPS: {fps.ToString("N0")} @ {msgCount.ToString("N0")} Msgs/{Math.Round(elapsedTime, 2)} Seconds",this);

        OnFPS?.Invoke(_startTime, msgCount);// doesn't work :(
    }

    [ContextMenu("Test Send")]
    private void TestSend()
    {
        _startTime = DateTime.UtcNow;
        StartCoroutine(Post(serverURL, jsonExample));
    }
}
