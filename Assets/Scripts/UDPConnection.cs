using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class UDPConnection
{
    private UdpClient udpClient;

    private readonly ConcurrentQueue<string> incomingQueue = new ConcurrentQueue<string>();
    private Thread receiveThread;
    private bool threadRunning = false;
    private string senderIp;
    private int senderPort;

    /// <summary>
    /// Starts The UDP Connection Listening
    /// </summary>
    /// <param name="sendIp">Host IP</param>
    /// <param name="sendPort">Sending Port (Host Port)</param>
    /// <param name="receivePort">Receiving Port</param>
    public void StartConnection(string sendIp, int sendPort, int receivePort)
    {
        try { udpClient = new UdpClient(receivePort); }
        catch (Exception e)
        {
            Debug.Log("Failed to listen for UDP at port " + receivePort + ": " + e.Message);
            return;
        }
        senderIp = sendIp;
        senderPort = sendPort;
        udpClient.EnableBroadcast = true; //for multicasting

        Debug.Log($"Sending through{sendIp}:{sendPort}. Receiving at:{receivePort}");

        StartReceiveThread();
        void StartReceiveThread()
        {
            receiveThread = new Thread(() => ListenForMessages(udpClient))
            {
                IsBackground = true
            };
            threadRunning = true;
            receiveThread.Start();
        }
    }
    /// <summary>
    /// Asynchronously reads all pending messages in the buffer
    /// </summary>
    /// <returns>An ASCII String of whatever is in the buffer at the call</returns>
    public async Task<string> FetchBuffer()
    {
        var receivedResults = await udpClient.ReceiveAsync();
        var rcvdMessage = Encoding.ASCII.GetString(receivedResults.Buffer);
        return rcvdMessage;
    }
    private void ListenForMessages(UdpClient client)
    {
        IPEndPoint remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
        Debug.Log($"Listening on port{remoteIpEndPoint.Port}...");
        while (threadRunning)
        {
            try
            {
                byte[] receiveBytes = client.Receive(ref remoteIpEndPoint); // Blocks until a message returns on this socket from a remote host.
                string returnData = Encoding.UTF8.GetString(receiveBytes);
                //Debug.Log($"rcvd: {returnData}");

                lock (incomingQueue)
                {
                    incomingQueue.Enqueue(returnData);
                }
            }
            catch (SocketException e)
            {
                // 10004 thrown when socket is closed
                if (e.ErrorCode != 10004)
                {
                    Debug.Log("Socket closed: " + e.Message);
                }
            }
            catch (Exception e)
            {
                Debug.Log("Error receiving: " + e.Message);
            }
            Thread.Sleep(1);
        }
    }

    /// <summary>
    /// Returns true if the incoming queue has one or more messages
    /// </summary>
    public bool HasMessages => incomingQueue.Count > 0;

    /// <summary>
    /// Checks if the connection is alive
    /// </summary>
    public bool IsAlive => udpClient.Available > 0;

    /// <summary>
    /// Dequeues messages from the concurrent queue
    /// </summary>
    /// <returns>String array of messages</returns>
    public async Task<string[]> GetMessages()
    {
        string[] pendingMessages = new string[0];
        lock (incomingQueue)
        {
            pendingMessages = new string[incomingQueue.Count];
            int i = 0;

            while (incomingQueue.TryPeek(out string receivedMessage))
            {
                if (incomingQueue.TryDequeue(out receivedMessage))
                {
                    pendingMessages[i] = receivedMessage;
                    i++;
                }
            }
        }
        return pendingMessages;
    }

    /// <summary>
    /// Sends a message to the connection endpoint
    /// </summary>
    /// <param name="message">Message as a string</param>
    public void SendMessage(string message)
    {
        Debug.Log(string.Format("Send msg to ip:{0} port:{1} msg:{2}", senderIp, senderPort, message));
        IPEndPoint serverEndpoint = new IPEndPoint(IPAddress.Parse(senderIp), senderPort);
        byte[] sendBytes = Encoding.UTF8.GetBytes(message);
        udpClient.Send(sendBytes, sendBytes.Length, serverEndpoint);
    }

    /// <summary>
    /// Closes the connection and stops receving thread
    /// </summary>
    public void StopConnection()
    {
        threadRunning = false;
        receiveThread.Abort();
        udpClient.Close();
    }
}
