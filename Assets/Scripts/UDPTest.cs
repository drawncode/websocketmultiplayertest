using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

public class UDPTest : MonoBehaviour
{
    //Server: UDPServer.js
    [SerializeField] private int sendPort = 5500;
    [SerializeField] private string serverIP = "127.0.0.1";
    [SerializeField] private Button testSendButton;

    private UDPConnection connection;
    private int ctr;
    private DateTime startTime;
    private int msgSizeInBits;

    public static event Action<DateTime, int, int> OnFPS = delegate { };
    public static List<PlayerData> PlayersData { get; private set; }

    private void Start()
    {
        int receivePort = 11000;

        connection = new UDPConnection();
        connection.StartConnection(serverIP, sendPort, receivePort);

        testSendButton?.onClick.AddListener(TestSend);
    }

    private void OnDestroy() => connection.StopConnection();

    private void Update()
    {
        PrintMsgs();
    }


    private async Task PrintMsgs()
    {
        if (connection.HasMessages)
        {
            startTime = DateTime.UtcNow;
            ctr = 0;
            var msgs = await connection.GetMessages();
            foreach (var message in msgs)
            {
                Debug.Log($"msg: [{++ctr}]: {message}");
                //var PlayerData = JsonConvert.DeserializeObject<PlayerData>(message);
                //Debug.Log(PlayerData.PlayerID);
            }

            //Debug.Log($"{ctr} msgs @ {DateTime.UtcNow}");
            
                if (ctr >= 20)//unreachable due to udp nature
            {
                msgSizeInBits = Encoding.ASCII.GetByteCount(msgs.ToString())*8;
                OnFPS?.Invoke(startTime,msgSizeInBits, ctr);//Set a public static 
            }
        }
    }

    [ContextMenu("Test Send")]
    private void TestSend()
    {
        connection.SendMessage("test2");
    }

    [ContextMenu("Test Receive")]
    private async Task TestRcv()
    {
        connection.SendMessage("test2");
        print("Receiving...");
        var msgs = await connection.FetchBuffer();
        print($"Rcvd: {msgs}");

    }
}
