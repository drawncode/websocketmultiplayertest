using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class UDP_ConnectionManager : MonoBehaviour
{
    [SerializeField] private string serverIP = "127.0.0.1";
    [SerializeField] private int serverPort = 5500;
    private UdpClient client;
    public static Action<bool> onComplete;
    private IPEndPoint remoteEndPoint;

    private void Awake() => UDPConnect(serverIP, serverPort);

    private void UDPConnect(string serverIP, int serverPort)
    {
        Debug.Log($"Connecting to:{serverIP}:{serverPort}...");
        client = new UdpClient();

        try
        {
            client.Connect(serverIP, serverPort);
            Debug.Log("Connected");
            UDPSend($"Incoming connection from {Environment.MachineName}");
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    private void xUpdate()
    {
        remoteEndPoint = new IPEndPoint(IPAddress.Any, serverPort);
        byte[] receiveBytes = client.Receive(ref remoteEndPoint);
        string receivedString = Encoding.ASCII.GetString(receiveBytes);
        Debug.Log($"rcvd: {receivedString}");
    }

    private void UDPSend(string msg)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
        client.Send(sendBytes, sendBytes.Length);
    }
    [ContextMenu("UDP Send Hi")]
    public void UDPSendHi() => UDPSend("Hi");
}
