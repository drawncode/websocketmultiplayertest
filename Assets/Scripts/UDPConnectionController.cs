using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UDPConnectionController : MonoBehaviour
{
    private UdpClient udpClient;
    private IPEndPoint remoteEndPoint;

    [SerializeField] private int serverPort = 5500;
    [SerializeField] private string serverIP = "127.0.0.1";
    [SerializeField] private TMP_Text fpsText;
    [SerializeField] private Transform cube;
    [SerializeField] private Button testButton;

    private float lastFPS;
    private float fPSUpdateThresh = 1f;
    private List<double> rcvTimes = new List<double>();

    public event Action<string> OnRcvd = delegate { };


    private void Start()
    {
        testButton.onClick.AddListener(UDPTestFPS);

        remoteEndPoint = new IPEndPoint(IPAddress.Parse(serverIP), serverPort);
        try
        {
            udpClient = new UdpClient();
            udpClient.Connect(remoteEndPoint);
            //UDPSend($"Client {Environment.MachineName} Connected!");
            Debug.Log("Connected, Receiving...");
            //socket.BeginReceive(ReceiveCallback, null);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    public async void TestFPS(string testData)
    {
        SendMessage(testData);

        var startTime = Time.time;
        int ctr = 0;
        while (udpClient.Available > 0)
        {
            //visualize during waiting
            cube.Rotate(Vector3.up * Time.deltaTime * 250);

            var receivedResults = await udpClient.ReceiveAsync();
            var rcvdMessage = Encoding.ASCII.GetString(receivedResults.Buffer);
            //Debug.Log(rcvdMessage);

            //await Task.Yield();
            if (receivedResults.Buffer.Length > 3)
            {
                ctr++;
            }
        }
        var timeElapsed = Math.Round(Time.time - startTime, 2);
        var fps = Math.Round(ctr / timeElapsed);
        if (ctr < 1)
        {
            return;
        }

        //Debug.Log($"{fps} FPS. {ctr} msgs @ {timeElapsed} seconds");
        Debug.Log($"msg: {testData}");

        fpsText.text = $"{fps} FPS. {ctr} msgs @ {timeElapsed} seconds";
    }


    private void ReceiveCallback(IAsyncResult result)
    {
        //startTime = Time.time;
        byte[] receiveBytes = udpClient.EndReceive(result, ref remoteEndPoint);
        string receivedString = Encoding.ASCII.GetString(receiveBytes);
        Debug.Log($"rcvd: {receivedString}");
        udpClient.BeginReceive(ReceiveCallback, null);
        //LogServerTime(startTime);
    }


    private void LogServerTime(float startTime)
    {
        var endTime = Time.time;
        var elapsedTime = endTime - startTime;
        var currentFPS = Mathf.Round(1 / elapsedTime);
        if (Mathf.Abs(currentFPS - lastFPS) < fPSUpdateThresh)
        {
            fpsText.text = "Network FPS: " + currentFPS.ToString("000");
        }
        lastFPS = currentFPS;
        //Debug.Log($"Time Elapsed{elapsedTime}, i.e.{currentFPS}");
    }

    private void SendMessage(string msg)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(msg);
        udpClient.Send(sendBytes, sendBytes.Length);
    }
    [ContextMenu("UDP Send TEST")]
    public void UDPSendHi() => SendMessage("TEST");


    [ContextMenu("Test FPS")]
    public void UDPTestFPS()
    {
        var playerData = new PlayerData(
                GetInstanceID(),
                transform.localPosition.Shorten(2),
                transform.localRotation.eulerAngles.Shorten(2));

        string playerDataJson = JsonConvert.SerializeObject(playerData);


        TestFPS(playerDataJson);
    }
    [ContextMenu("Begin Rcv")]
    public void UDPRcv()
    {
        SendMessage("test2");
        udpClient.BeginReceive(ReceiveCallback, null);
    }
}
