using TMPro;
using UnityEngine;

public class UIConnectionStatus : MonoBehaviour
{
    TMP_Text ConnectionTxt;
    private void Awake() => ConnectionTxt = GetComponent<TMP_Text>();

    private void OnEnable() => WS4Net.OnConnectionStatus += WSClient_ONConnectionStatus;
    private void OnDisable() => WS4Net.OnConnectionStatus -= WSClient_ONConnectionStatus;
    private void WSClient_ONConnectionStatus(bool status) => ConnectionTxt.enabled = !status;
}
