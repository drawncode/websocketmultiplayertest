using System;
using TMPro;
using UnityEngine;

public class UIFps : MonoBehaviour
{
    TMP_Text FpsText;
    private void Awake() => FpsText = GetComponent<TMP_Text>();

    private void OnEnable()
    {
        WS4Net.OnFPS += WSClient_OnFPS;
        UDPTest.OnFPS += WSClient_OnFPS;
    }

    private void OnDisable()
    {
        WS4Net.OnFPS -= WSClient_OnFPS;
        UDPTest.OnFPS -= WSClient_OnFPS;
    }

    private void WSClient_OnFPS(DateTime startTime, int msgSizeInBits, int msgCount)
    {
        var endTime = DateTime.UtcNow;
        var elapsedTime = (endTime - startTime).TotalSeconds;

        var fps = Math.Round(msgCount / elapsedTime, 3);

        //var msgSizeInMBs = (msgSizeInBits * msgCount) / (10 ^ 6);//converting to MBs (Original logic)
        var msgSizeInMBs = ((msgSizeInBits / 8) * msgCount) / (8 * (10 ^ 6));//converting back to bytes as per beppe's formula
        var mbps = msgSizeInMBs / elapsedTime;
        FpsText.text = ($"FPS: {fps.ToString("N0")}\n" +
            $"@ {msgCount.ToString("N0")} Msgs/{Math.Round(elapsedTime, 2)} Seconds\n" +
            $"Baud Rate: {mbps.ToString("N0")} MBs/Sec");
    }
}
