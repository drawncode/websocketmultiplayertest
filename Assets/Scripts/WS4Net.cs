using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WebSocket4Net;

public class WS4Net : MonoBehaviour
{
    [SerializeField] private string iP = "127.0.0.1";
    [SerializeField] private int testSize; //Used for testing network speed
    [SerializeField] private Button testButton; 
    [SerializeField] private Transform playerTransform;
    [SerializeField][Range(0.01f, 1f)] private float networkSmoothingFactor = 0.5f; //1 being real-time
    [SerializeField] private bool testMode;

    private WebSocket ws;
    private int ctr;
    private DateTime startTime;
    private int msgSizeInBits;
    private ConcurrentQueue<string> incomingQueue = new ConcurrentQueue<string>();

    public static event Action<DateTime, int, int> OnFPS = delegate { };
    public static event Action<bool> OnConnectionStatus = delegate { };

    private void Start()
    {
        CreateSocket();
        testButton.onClick.AddListener(StartTest);
    }

    private void Update()
    {
        if (testMode)
            TestNetworkSpeed();
        else
            MovePlayer();
    }

    /// <summary>
    /// Connects to WSServer.js found on the iP field and listening on port 8080
    /// </summary>
    private void CreateSocket()
    {
        ws = new WebSocket("ws://" + iP + ":8080");
        ws.MessageReceived += ON_MessageReceived;
        OnConnectionStatus(true);
        ws.Open();
    }

    /// <summary>
    /// Triggered on message received and is used here to fill the concurrent queue for thread safety
    /// </summary>
    /// <param name="sender">Not used here</param>
    /// <param name="e">Contains the message</param>
    private void ON_MessageReceived(object sender, EventArgs e)
    {
        ctr++;
        var rcvdMessage = ((MessageReceivedEventArgs)e).Message;

        if (ctr == 1)
        {
            startTime = DateTime.UtcNow;
            msgSizeInBits = Encoding.ASCII.GetByteCount(rcvdMessage)*8;
        }

        //Debug.Log($"rcvd[{ctr}]: {rcvdMessage}");
        incomingQueue.Enqueue(rcvdMessage);
    }

    /// <summary>
    /// Moving and rotating players using data received from server
    /// </summary>
    private void MovePlayer()
    {
        if (incomingQueue.TryDequeue(out string receivedMessage))
        {
            var playerData_Recieved = JsonConvert.DeserializeObject<List<PlayerData>>(receivedMessage);
            foreach (var playerData in playerData_Recieved)
            {
                //if (playerTransform.GetInstanceID() == playerData.PlayerID) enable to move a specific player in the scene
                {
                    playerTransform.position = Vector3.Slerp(playerTransform.position, playerData.Pos, networkSmoothingFactor * Time.deltaTime);
                    playerTransform.eulerAngles = Vector3.Slerp(playerTransform.eulerAngles, playerData.Rot, networkSmoothingFactor * Time.deltaTime);
                }
            }
        }
    }

    private void TestNetworkSpeed()
    {
        //for visualization during receiving messages in test mode
        if (ctr > 0)
            playerTransform?.Rotate(Vector3.up * Time.deltaTime * 250);

        //msg size is ~82 bytes x 10 players x 8 bit = 6,560 bits
        if (ctr >= testSize)
        {
            OnFPS?.Invoke(startTime, msgSizeInBits, ctr);
            ctr = 0;
        }
    }

    [ContextMenu("Start Test")]
    private void StartTest()
    {
        if(ws.State == WebSocketState.Closed)
            ws.Open();
        
        ws?.Send("Start");
    }

    private void OnApplicationQuit()
    {
        ws.Send("EOT");
        ws.MessageReceived -= ON_MessageReceived;
        ws.Close();
        OnConnectionStatus(false);
    }
}
