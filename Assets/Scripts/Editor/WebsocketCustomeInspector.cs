using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExpressServerManager))]
public class WebsocketCustomeInspector : Editor
{
    bool serverStatus;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space(20);
        GUILayout.Label("Server");
        GUILayout.BeginHorizontal();

        GUI.enabled = ExpressServerManager.serverStatus == ServerStatus.Stopped;
        if (GUILayout.Button("Start"))
        {
            ExpressServerManager.StartServer();
        }
        GUI.enabled = true;// SyncManager.serverStatus == ServerStatus.Running;
        if (GUILayout.Button("Stop"))
        {
            ExpressServerManager.StopServer();
        }
        GUILayout.EndHorizontal();
    }
    }
