using UnityEngine;

public class ColorController : MonoBehaviour
{
    [SerializeField] Color[] cubeColors;
    void Start()
    {
        var mat = GetComponent<Renderer>().material;
        int index = Random.Range(0, cubeColors.Length - 1);
        mat.color = cubeColors[index];
    }
}
